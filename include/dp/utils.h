#ifndef __UTILS_H__
#define __UTILS_H__

#include <vector>
#include <string>

#ifndef WITHOUT_LIBPLIST

std::vector<uint8_t> ExtractKeyBagFromPlist(std::string plistPath);

template<class _Dtype, class _Btype>
_Dtype *safe_plist_cast(_Btype *p) {
    _Dtype *ret = dynamic_cast<_Dtype *>(p);
    if (ret == nullptr) {
        fprintf(stderr, "cannot cast to %s, type = %d\n", typeid(_Dtype *).name(), p->GetType());
        exit(-1);
    }
    return ret;
}

#endif // WITHOUT_LIBPLIST

#endif

