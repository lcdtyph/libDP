#ifndef __KEYBAG_H__
#define __KEYBAG_H__

#include <unordered_map>
#include <map>
#include <string>
#include <vector>
#include <memory>
#include <mutex>
#include <boost/any.hpp>
#include <boost/thread.hpp>

#include <stdio.h>

struct Pbkdf2Option {
    std::string hash_spec;
    std::vector<uint8_t> salt;
    uint32_t iters;
    uint32_t derived_length;
};

class BackupKeyBag {
public:
    using bytes = std::vector<uint8_t>;
    using KeyBagItem = std::map<std::string, boost::any>;

    static std::shared_ptr<BackupKeyBag>
        FromByteSequence(const bytes &buf);
    static std::shared_ptr<BackupKeyBag>
        FromString(const std::string &buf);
    static std::shared_ptr<BackupKeyBag>
        UnlockedFromBytes(const bytes &buf);

#ifndef WITHOUT_FASTPBKDF2
    bytes GeneratePasscodeKey(const bytes &psc) const;

    bool UnlockWithPasscode(std::string passcode);
    bool UnlockWithPasscode(const bytes &passcode);
#endif // WITHOUT_FASTPBKDF2

    bool UnlockWithPasscodeKey(const bytes &key);

    bool UnwrapKeyOfClass(uint32_t clas, const bytes &key, bytes &out) const;
    void DumpToFp(FILE *fp) const;
    std::string &DumpToStr(std::string &str) const;
    bool unlocked() const;
    Pbkdf2Option GetFirstKdfOption() const;
    Pbkdf2Option GetSecondKdfOption() const;
    bytes DumpUnlockedKeybag() const;

private:
    BackupKeyBag() = default;

    KeyBagItem attributes_;
    std::map<std::string, KeyBagItem> classItems_;
    mutable boost::shared_mutex keyMutex_;
    std::unordered_map<uint32_t, bytes> classKeys_;
};

#endif

