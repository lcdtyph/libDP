#ifndef __CRYPTO_H__
#define __CRYPTO_H__

#include <vector>
#include <stdint.h>

bool AESUnwrap(const std::vector<uint8_t> &key, const std::vector<uint8_t> &data, std::vector<uint8_t> &output);

#ifndef WITHOUT_FASTPBKDF2
std::vector<uint8_t> pbkdf2_sha1(const std::vector<uint8_t> &passcode, const std::vector<uint8_t> &salt, int iters, int n);
std::vector<uint8_t> pbkdf2_sha256(const std::vector<uint8_t> &passcode, const std::vector<uint8_t> &salt, int iters, int n);
#endif // WITHOUT_FASTPBKDF2

#endif

