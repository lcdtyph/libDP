
#include "dp/keybag.h"
#include "dp/crypto.h"
#include <exception>
#include <sstream>
#include <boost/format.hpp>
#include <boost/endian/conversion.hpp>

#include "keybag.pb.h"

using bytes = BackupKeyBag::bytes;
using KeyBagItem = BackupKeyBag::KeyBagItem;

#ifndef WITHOUT_FASTPBKDF2
bytes BackupKeyBag::GeneratePasscodeKey(const bytes &psc) const {
    auto first = GetFirstKdfOption();
    auto second = GetSecondKdfOption();
    return pbkdf2_sha1(pbkdf2_sha256(psc, first.salt, first.iters, first.derived_length),
                       second.salt, second.iters, second.derived_length);
}

bool BackupKeyBag::UnlockWithPasscode(std::string passcode) {
    return UnlockWithPasscode(bytes{passcode.begin(), passcode.end()});
}

bool BackupKeyBag::UnlockWithPasscode(const bytes &psc) {
    return UnlockWithPasscodeKey(GeneratePasscodeKey(psc));
}
#endif // WITHOUT_FASTPBKDF2

bool BackupKeyBag::unlocked() const {
    boost::shared_lock<boost::shared_mutex> _{keyMutex_};
    return !classKeys_.empty();
}

Pbkdf2Option BackupKeyBag::GetFirstKdfOption() const {
    Pbkdf2Option result;
    result.hash_spec = "sha256";
    result.salt = boost::any_cast<bytes>(attributes_.at("DPSL"));
    result.iters = boost::any_cast<uint32_t>(attributes_.at("DPIC"));
    result.derived_length = 32;
    return result;
}

Pbkdf2Option BackupKeyBag::GetSecondKdfOption() const {
    Pbkdf2Option result;
    result.hash_spec = "sha1";
    result.salt = boost::any_cast<bytes>(attributes_.at("SALT"));
    result.iters = boost::any_cast<uint32_t>(attributes_.at("ITER"));
    result.derived_length = 32;
    return result;
}

bool BackupKeyBag::UnwrapKeyOfClass(uint32_t clas, const bytes &key, bytes &out) const {
    if (!unlocked()) throw std::runtime_error("unlocked keybag");
    boost::shared_lock<boost::shared_mutex> _{keyMutex_};
    auto itr = classKeys_.find(clas);
    if (itr == classKeys_.end()) throw std::runtime_error("invalid class");
    if (key.size() != 0x28) throw std::runtime_error("invalid key length");
    return AESUnwrap(itr->second, key, out);
}

bool BackupKeyBag::UnlockWithPasscodeKey(const bytes &key) {
    std::unordered_map<uint32_t, bytes> tempKeys;
    for (auto &itemKV : classItems_) {
        auto &item = itemKV.second;
        auto WPKY = boost::any_cast<bytes>(item["WPKY"]);
        auto CLAS = boost::any_cast<uint32_t>(item["CLAS"]);
        auto WRAP = boost::any_cast<uint32_t>(item["WRAP"]);
        if (WRAP != 2) continue;
        if (!AESUnwrap(key, WPKY, tempKeys[CLAS])) {
            return false;
        }
    }
    {
        boost::unique_lock<boost::shared_mutex> _{keyMutex_};
        classKeys_ = std::move(tempKeys);
    }
    return true;
}

static std::map<std::string, KeyBagItem> __ParseKeyBag(const std::string &buf, uint32_t *vers, uint32_t *type);

std::shared_ptr<BackupKeyBag> BackupKeyBag::FromByteSequence(const bytes &buf) {
    return BackupKeyBag::FromString(std::string{buf.begin(), buf.end()});
}

std::shared_ptr<BackupKeyBag> BackupKeyBag::FromString(const std::string &buf) {
    uint32_t vers, type;
    auto dataDict = __ParseKeyBag(buf, &vers, &type);
    if (vers != 3) throw std::runtime_error("unsupported keybag version");
    if (type != 1) throw std::runtime_error("unsupported keybag type");
    std::shared_ptr<BackupKeyBag> result;
    result.reset(new BackupKeyBag());
    for (auto &itemKV : dataDict) {
        auto &item = itemKV.second;
        auto itr = item.find("CLAS");
        if (itr == item.end()) {
            result->attributes_ = std::move(item);
        } else {
            result->classItems_[itemKV.first] = std::move(item);
        }
    }
    return result;
}

std::map<std::string, KeyBagItem> __ParseKeyBag(const std::string &buf, uint32_t *vers, uint32_t *type) {
    std::map<std::string, KeyBagItem> result;
    std::istringstream iss(buf);
    char tempBuf[5] = "";
    uint32_t tempInt;
    auto itr = result.end();

    while (iss.read(tempBuf, 4) && iss.gcount() == 4) {
        iss.read((char *)&tempInt, 4);
        if (iss.gcount() != 4) {
            fprintf(stderr, "incomplete keybag, rest data: %s\n", tempBuf);
            goto __end;
        }
        tempInt = boost::endian::big_to_native(tempInt);

        if (tempInt == 4) {
            iss.read((char *)&tempInt, 4);
            if (iss.gcount() != 4) {
                fprintf(stderr, "incomplete keybag, rest data: %s\n", tempBuf);
                goto __end;
            }
            tempInt = boost::endian::big_to_native(tempInt);

            if (strncmp(tempBuf, "VERS", 4) == 0) {
                *vers = tempInt;
            } else if(strncmp(tempBuf, "TYPE", 4) == 0) {
                *type = tempInt;
            } else {
                itr->second[tempBuf] = boost::any(tempInt);
            }
        } else {
            std::vector<uint8_t> tempData(tempInt);
            iss.read((char *)tempData.data(), tempInt);
            if (iss.gcount() != tempInt) {
                fprintf(stderr, "incomplete keybag, rest data: %s\n", tempBuf);
                goto __end;
            }
            if (strncmp(tempBuf, "UUID", 4) == 0) {
                auto res = result.insert({std::string(std::begin(tempData), std::end(tempData)), KeyBagItem()});
                if (!res.second) {
                    fprintf(stderr, "cannot insert key, rest data: %s\n", tempBuf);
                    goto __end;
                }
                itr = res.first;
            } else {
                itr->second[tempBuf] = boost::any(std::move(tempData));
            }
        }
    }

__end:
    return result;
}

static void __DumpKeyBagItem(const KeyBagItem &item, std::ostream &os) {
    for (auto &j : item) {
        os << boost::format(", %1%: ") % j.first;
        if (j.second.type() == typeid(uint32_t)) {
            uint32_t ui = boost::any_cast<uint32_t>(j.second);
            os << boost::format("%#x") % ui;
        } else {
            auto buf = boost::any_cast<std::vector<uint8_t>>(j.second);
            for (int32_t c : buf) {
                os << boost::format("%02hhx") % (c & 0xff);
            }
        }
    }
    os << std::endl;
}

void BackupKeyBag::DumpToFp(FILE *fp) const {
    std::string buf;
    fputs(DumpToStr(buf).c_str(), fp);
}

std::string &BackupKeyBag::DumpToStr(std::string &str) const {
    str.clear();
    std::ostringstream oss;
    oss << "Attributes";
    __DumpKeyBagItem(attributes_, oss);
    oss << "----------------------------------------------------------\n";
    for (auto &itemKV : classItems_) {
        oss << "UUID: ";
        for (auto c : itemKV.first) {
            oss << boost::format("%02hhx") % (((uint32_t)c) & 0xff);
        }
        __DumpKeyBagItem(itemKV.second, oss);
    }
    if (unlocked()) {
        boost::shared_lock<boost::shared_mutex> _{keyMutex_};
        oss << "unlocked key data:\n";
        for (auto &key : classKeys_) {
            oss << boost::format("CLASS: %1%, KEY: ") % key.first;
            for (int32_t c : key.second) {
                oss << boost::format("%02hhx") % (c & 0xff);
            }
            oss << std::endl;
        }
    }
    return (str = oss.str());
}

std::shared_ptr<BackupKeyBag>
    BackupKeyBag::UnlockedFromBytes(const bytes &buf) {
        std::shared_ptr<BackupKeyBag> result;
        keybag::UnlockedKeybag keybag_proto;
        if (!keybag_proto.ParseFromArray(buf.data(), buf.size())) {
            return nullptr;
        }
        result.reset(new BackupKeyBag);
        auto first_option = keybag_proto.first_option();
        result->attributes_["DPIC"] = boost::any(first_option.iters());
        result->attributes_["DPSL"] = boost::any(first_option.salt());
        auto second_option = keybag_proto.second_option();
        result->attributes_["ITER"] = boost::any(second_option.iters());
        result->attributes_["SALT"] = boost::any(second_option.salt());

        auto class_keys = keybag_proto.classkeys();
        for (auto &kv : class_keys) {
            std::pair<uint32_t, bytes> new_kv;
            new_kv.first = kv.first;
            new_kv.second = bytes{kv.second.begin(), kv.second.end()};
            result->classKeys_.insert(new_kv);
        }
        return result;
    }

bytes BackupKeyBag::DumpUnlockedKeybag() const {
    if (!unlocked()) {
        throw std::runtime_error("Keybag is still locked");
    }
    keybag::UnlockedKeybag keybag_proto;
    auto option = GetFirstKdfOption();
    auto option_proto = keybag_proto.mutable_first_option();
    option_proto->set_iters(option.iters);
    option_proto->set_salt(option.salt.data(), option.salt.size());
    option = GetSecondKdfOption();
    option_proto = keybag_proto.mutable_second_option();
    option_proto->set_iters(option.iters);
    option_proto->set_salt(option.salt.data(), option.salt.size());

    auto classkeys = keybag_proto.mutable_classkeys();
    {
        boost::shared_lock<boost::shared_mutex> _{keyMutex_};
        for (auto &kv : classKeys_) {
            std::string value{kv.second.begin(), kv.second.end()};
            (*classkeys)[kv.first] = std::move(value);
        }
    }
    std::string buf;
    keybag_proto.SerializeToString(&buf);
    return bytes{buf.begin(), buf.end()};
}

