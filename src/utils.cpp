
#include "dp/utils.h"
#include <algorithm>
#include <iterator>

#ifndef WITHOUT_LIBPLIST
#include <plist/plist++.h>

std::vector<uint8_t> ExtractKeyBagFromPlist(std::string plistPath) {
    FILE *ifp = fopen(plistPath.c_str(), "rb");
    if (ifp == nullptr) {
        fprintf(stderr, "cannot open file: %s\n", plistPath.c_str());
        exit(-1);
    }

    fseek(ifp, 0, SEEK_END);
    std::vector<char> buf(ftell(ifp));
    rewind(ifp);

    fread(buf.data(), buf.size(), 1, ifp);
    fclose(ifp);

    std::unique_ptr<PList::Structure> plist;
    plist.reset(PList::Structure::FromBin(buf));

    auto dict = safe_plist_cast<PList::Dictionary>(plist.get());
    auto keybag = safe_plist_cast<PList::Data>((*dict)["BackupKeyBag"]);

    auto value = keybag->GetValue();
    std::vector<uint8_t> result;
    std::copy(std::begin(value), std::end(value), std::back_inserter(result));
    return result;
}

#endif // WITHOUT_LIBPLIST

