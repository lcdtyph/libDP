
#include "dp/crypto.h"
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <stdexcept>
#include <stdint.h>

bool AESUnwrap(const std::vector<uint8_t> &key, const std::vector<uint8_t> &data, std::vector<uint8_t> &output) {
    if (data.size() < 8) throw std::runtime_error("invalid wrapped data");
    std::vector<uint8_t> result(data.size() - 8);
    uint64_t iv = 0xA6A6A6A6A6A6A6A6;
    AES_KEY userKey;
    int ret = AES_set_decrypt_key(key.data(), key.size() * 8, &userKey);
    if (ret) throw std::runtime_error("invalid KEK");

    ret = AES_unwrap_key(&userKey, (const uint8_t *)&iv, result.data(), data.data(), data.size());
    if (ret <= 0) return false;
    output = std::move(result);
    return true;
}

#ifndef WITHOUT_FASTPBKDF2
#include "fastpbkdf2.h"
std::vector<uint8_t> pbkdf2_sha1(const std::vector<uint8_t> &passcode, const std::vector<uint8_t> &salt, int iters, int n) {
    std::vector<uint8_t> result(n);
    fastpbkdf2_hmac_sha1(passcode.data(), passcode.size(),
                         salt.data(), salt.size(),
                         iters, result.data(), n);
    return result;
}

std::vector<uint8_t> pbkdf2_sha256(const std::vector<uint8_t> &passcode, const std::vector<uint8_t> &salt, int iters, int n) {
    std::vector<uint8_t> result(n);
    fastpbkdf2_hmac_sha256(passcode.data(), passcode.size(),
                         salt.data(), salt.size(),
                         iters, result.data(), n);
    return result;
}
#endif // WITHOUT_FASTPBKDF2

