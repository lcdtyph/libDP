cmake_minimum_required(VERSION 3.0)
project(libDP)

set(CMAKE_CXX_STANDARD 14)

option(BUILD_WITH_FASTPBKDF2 "If build with fastpbkdf2" ON)

if (NOT BUILD_WITH_FASTPBKDF2)
    add_definitions(-DWITHOUT_FASTPBKDF2)
else()
    add_subdirectory(fastpbkdf2)
    set(DEPS fastpbkdf2)
endif()

if (APPLE)
    find_package(PkgConfig REQUIRED)
    pkg_search_module(LIBPLIST REQUIRED libplist++)
    list(APPEND DEPS ${LIBPLIST_LDFLAGS})

    set(OPENSSL_ROOT_DIR "/usr/local/opt/openssl@1.1")
else()
    add_definitions(-DWITHOUT_LIBPLIST)
endif()

set(OPENSSL_USE_STATIC_LIBS TRUE)
find_package(OpenSSL REQUIRED)
include_directories(${OPENSSL_INCLUDE_DIR})

find_package(Protobuf REQUIRED)
include_directories(${Protobuf_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
protobuf_generate_cpp(PROTO_SRC PROTO_HDR proto/keybag.proto)

find_package(Boost REQUIRED COMPONENTS system thread)
include_directories(${Boost_INCLUDE_DIRS})

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

list(APPEND DEPS OpenSSL::Crypto ${Protobuf_LIBRARIES} ${Boost_LIBRARIES})

set(SOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/src/crypto.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/keybag.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/utils.cpp
    ${PROTO_SRC}
    ${PROTO_HDR}
   )

add_library(DP STATIC ${SOURCES})
target_link_libraries(DP ${DEPS})
target_include_directories(DP PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

